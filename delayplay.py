#!/bin/python

import webbrowser
import socket
import requests
from requests_oauthlib import OAuth2Session

class OAuthService:
	def __init__(self):
		with open("./client_id") as f:
			self.client_id = f.readline()

		with open("./client_secret") as f:
			self.client_secret = f.readline()

		self.client_id = self.client_id.strip()
		self.client_secret = self.client_secret.strip()

		self.host = "localhost"
		self.port = "4563"
		self.api_base = "https://accounts.spotify.com"
		self.redirect_uri = "http://" + self.host + ":" + self.port + "/spotify"
		self.scope = ["streaming"]
		self.oauth = OAuth2Session(self.client_id, redirect_uri=self.redirect_uri, scope=self.scope)
		auth_url, state = self.oauth.authorization_url("https://accounts.spotify.com/authorize/")
		print("Please go to " + auth_url)
		auth_response = input("Enter the full callback URL:\n")
		self.token = self.oauth.fetch_token("https://accounts.spotify.com/api/token/", client_secret=self.client_secret, code="authorization_code")
		print(self.token)



def main():
	oauth = OAuthService()

if __name__ == '__main__':
	main()
